import { Message } from "discord.js";
import { PingFinder } from "../commands/ping-finder";
import { inject, injectable } from "inversify";
import { TYPES } from "../types";
import { PingServer } from "../commands/ping-server";
import { Client } from "discord.js";
import { resolve } from "dns";

@injectable()
export class MessageResponder {
  private pingFinder: PingFinder;
  private pingServer: PingServer;
  private prefix: string;

  constructor(
    @inject(TYPES.PingFinder) pingFinder: PingFinder,
    @inject(TYPES.PingServer) pingServer: PingServer,
    @inject(TYPES.Prefix) prefix: string
  ) {
    this.pingFinder = pingFinder;
    this.pingServer = pingServer;
    this.prefix = prefix;
  }

  handle(message: Message, client: Client): Promise<Message | Message[]> {
    if (message.content[0] !== this.prefix) {
      return new Promise(res => {
        "Non prefix acceptence";
      });
    }
    const args = message.content.slice(this.prefix.length).split(" ");
    const command = args.shift().toLowerCase();

    if (this.pingFinder.isPing(command)) {
      message.react("🎮");
      message.react("👍");
      return message.reply("Pong!");
    }

    if (command == "server") {
      message.reply("Checking Server Status. Please wait!!!");
      return this.pingServer.pingNow(message, client);
    }

    return new Promise(res => {
      "Non prefix acceptence";
    });
  }
}

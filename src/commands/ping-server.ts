import { inject, injectable } from "inversify";
import { Message, PresenceStatus } from "discord.js";
import { TYPES } from "../types";
import { Client } from "discord.js";
import Axios from "axios";

@injectable()
export class PingServer {
  private jsonApi: string = "https://api.mcsrvstat.us/2/";
  // private jsonApi: string = "https://mcapi.us/server/status?ip=";
  // private test: string = "http://www.mocky.io/v2/5e474715330000b17a026781"; // hiting a json format in mocky.io
  // private test: string = "http://www.mocky.io/v2/5e4b289e2f00005c0097d75c";
  private host: string;
  private mcInfo: Object;

  constructor(@inject(TYPES.HostIP) host: string) {
    this.host = host;
    this.mcInfo = {
      online: false,
      players: {
        online: 0,
        max: 20,
        list: []
      }
    };
  }

  pingNow(
    messageContent: Message,
    client: Client
  ): Promise<Message | Message[]> {
    let mcInformation: Object = {};

    Axios.get(this.jsonApi + this.host)
      .then(res => {
        // here i have all the info of the mc_server
        mcInformation["online"] = res.data["online"];
        mcInformation["players"] = res.data["players"];
        // set the info to a class
        this.setmcInfo(mcInformation);
      })
      .catch(err => {
        console.log("this is the error: ", err);
      });

    if (this.mcInfo["online"] == true) {
      if (this.mcInfo["players"].online >= 1) {
        // One of more people are in the server
        return this.setServerStatus(
          this.mcInfo,
          client,
          messageContent,
          "online"
        );
      } else if (this.mcInfo["players"].online < 1) {
        // Server is online but no one is online
        return this.setServerStatus(
          this.mcInfo,
          client,
          messageContent,
          "idle"
        );
      }
    } else {
      // server is offline
      messageContent.react("😭");
      client.user.setActivity("Server offline!!!");
      client.user.setStatus("dnd");
      return messageContent.reply("Server is offline");
    }
  }

  private setmcInfo(o: Object) {
    if (o["players"] !== undefined) this.mcInfo["players"] = o["players"];
    if (o["online"] !== undefined) this.mcInfo["online"] = o["online"];
  }

  public getmcInfo() {
    return this.mcInfo;
  }

  private setServerStatus(
    mcData: Object,
    client: Client,
    message: Message,
    status: PresenceStatus
  ): Promise<Message | Message[]> {
    client.user.setActivity(
      mcData["players"].online + " of " + mcData["players"].max + " online"
    );
    message.react("🎮");
    client.user.setStatus(status);
    return message.reply("Server is online!!!");
  }
}

// {
//   "ip":"fdsfsdfsdf",
//   "port":25565,
//   "debug":{
//      "ping":true,
//      "query":false,
//      "srv":false,
//      "querymismatch":false,
//      "ipinsrv":false,
//      "animatedmotd":false,
//      "proxypipe":false,
//      "cachetime":0,
//      "api_version":"2",
//      "dns":{
//         "a":[

//         ]
//      }
//   },
//   "motd":{
//      "raw":[
//         "A Minecraft Server"
//      ],
//      "clean":[
//         "A Minecraft Server"
//      ],
//      "html":[
//         "A Minecraft Server"
//      ]
//   },
//   "players":{
//      "online":0,
//      "max":20
//   },
//   "version":"1.14.4",
//   "online":true,
//   "protocol":498,
//   "hostname":"ofsafrg.nfsdfas.net"
// }

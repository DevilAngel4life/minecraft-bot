// This is the link to add the bot to a server
// Copy and paste it in the browser. This will display the page to add
// to the server.

// https://discordapp.com/api/oauth2/authorize?client_id=676578056126726146&scope=bot&permissions=133184

require("dotenv").config(); // Recommended way of loading dotenv
import container from "../inversify.config";
import { TYPES } from "./types";
import { Bot } from "./bot";
let bot = container.get<Bot>(TYPES.Bot);

bot
  .listen()
  .then(() => {
    console.log("Logged in!");
  })
  .catch(error => {
    console.log("Oh no! ", error);
  });

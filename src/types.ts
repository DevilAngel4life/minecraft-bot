export const TYPES = {
  Bot: Symbol("Bot"),
  Client: Symbol("Client"),
  Token: Symbol("Token"),
  MessageResponder: Symbol("MessageResponder"),
  PingFinder: Symbol("PingFinder"),
  HostIP: Symbol("HostIP"),
  PingServer: Symbol("PingServer"),
  Prefix: Symbol("Prefix"),
  Collenction: Symbol("Collection")
};

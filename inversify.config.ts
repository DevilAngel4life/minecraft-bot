import "reflect-metadata";
import { Container } from "inversify";
import { TYPES } from "./src/types";
import { Bot } from "./src/bot";
import { Client, Collection } from "discord.js";
import { PingFinder } from "./src/commands/ping-finder";
import { MessageResponder } from "./src/services/message-responder";
import { PingServer } from "./src/commands/ping-server";

let container = new Container();

container
  .bind<Bot>(TYPES.Bot)
  .to(Bot)
  .inSingletonScope();
container.bind<Client>(TYPES.Client).toConstantValue(new Client());
container.bind<string>(TYPES.Token).toConstantValue(process.env.TOKEN);
container.bind<string>(TYPES.Prefix).toConstantValue(process.env.PREFIX);
container
  .bind<MessageResponder>(TYPES.MessageResponder)
  .to(MessageResponder)
  .inSingletonScope();
container
  .bind<PingFinder>(TYPES.PingFinder)
  .to(PingFinder)
  .inSingletonScope();
container.bind<string>(TYPES.HostIP).toConstantValue(process.env.HOSTIP);
container
  .bind<PingServer>(TYPES.PingServer)
  .to(PingServer)
  .inSingletonScope();

export default container;

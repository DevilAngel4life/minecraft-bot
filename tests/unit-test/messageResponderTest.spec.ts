// import "reflect-metadata";
// import "mocha";
// import { expect } from "chai";
// import { PingFinder } from "../../src/commands/ping-finder";
// import { MessageResponder } from "../../src/services/message-responder";
// import { instance, mock, verify, when } from "ts-mockito";
// import { Message } from "discord.js";
// import { PingServer } from "../../src/commands/ping-server";

// describe("MessageResponder", () => {
//   let mockedPingFinderClass: PingFinder;
//   let mockedPingFinderInstance: PingFinder;
//   let mockedPingServerClass: PingServer;
//   let mockedPingServerInstance: PingServer;
//   let mockedMessageClass: Message;
//   let mockedMessageInstance: Message;
//   let mockedPrefixInstance: string;

//   let service: MessageResponder;

//   beforeEach(() => {
//     mockedPingFinderClass = mock(PingFinder);
//     mockedPingFinderInstance = instance(mockedPingFinderClass);
//     mockedPingServerClass = mock(mockedPingServerClass);
//     mockedPingServerInstance = instance(mockedPingServerInstance);
//     mockedMessageClass = mock(Message);
//     mockedMessageInstance = instance(mockedMessageClass);

//     setMessageContents();

//     service = new MessageResponder(
//       mockedPingFinderInstance,
//       mockedPingServerInstance,
//       mockedPrefixInstance
//     );
//   });

//   it("should reply", async () => {
//     whenIsPingThenReturn(true);

//     await service.handle(mockedMessageInstance);

//     verify(mockedMessageClass.reply("pong!")).once();
//   });

//   it("should not reply", async () => {
//     whenIsPingThenReturn(false);

//     await service
//       .handle(mockedMessageInstance)
//       .then(() => {
//         // Successful promise is unexpected, so we fail the test
//         expect.fail("Unexpected promise");
//       })
//       .catch(() => {
//         // Rejected promise is expected, so nothing happens here
//       });

//     verify(mockedMessageClass.reply("pong!")).never();
//   });

//   // it("should not send message", async () => {
//   //   whenIsPingServerThen();

//   //   await service.handle(mockedMessageInstance);

//   //   verify(mockedMessageClass.reply("pong!")).once();
//   // });

//   function setMessageContents() {
//     mockedMessageInstance.content = "Non-empty string";
//   }

//   function whenIsPingThenReturn(result: boolean) {
//     when(mockedPingFinderClass.isPing("Non-empty string")).thenReturn(
//       result
//     );
//   }
//   // function whenIsPingServerThen() {
//   //   when(mockedPingServerClass.pingNow()).then;
//   // }
// });

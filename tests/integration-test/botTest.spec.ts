import container from "../../inversify.config";
import { TYPES } from "../../src/types";
import { Client } from "discord.js";
import { instance, mock, verify, when } from "ts-mockito";
import { Bot } from "../../src/bot";

describe("Bot", () => {
  let discordMock: Client;
  let discordInstance: Client;
  let bot: Bot;

  beforeEach(() => {
    discordMock = mock(Client);
    discordInstance = instance(discordMock);
    container
      .rebind<Client>(TYPES.Client)
      .toConstantValue(discordInstance);
    bot = container.get<Bot>(TYPES.Bot);
  });

  // Test cases here
});
